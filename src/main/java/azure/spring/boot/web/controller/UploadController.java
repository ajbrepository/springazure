package azure.spring.boot.web.controller;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.connector.Request;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UploadController {

	@Value(".")
	private String path;

	@RequestMapping(value = "/doUpload", method = RequestMethod.POST)
	public String upload(@RequestParam MultipartFile file) throws IOException {

		if (!file.isEmpty()) {

			try {
				String fileName = file.getOriginalFilename();
				InputStream is = file.getInputStream();
				Files.copy(is, Paths.get(System.getProperty("user.dir") + "\\" + fileName),
						StandardCopyOption.REPLACE_EXISTING);
				System.out.println(System.getProperty("user.dir"));
				return "redirect:/success.html";
			} catch (Exception e) {

				return "redirect:/failure.html";
			}

		} else {

			return "redirect:/failure.html";
		}
	}
	

	
	@ExceptionHandler(Throwable.class)
	  public ModelAndView handleError(HttpServletRequest req, Exception ex) {
	    System.out.println("Request: " + req.getRequestURL() + " raised " + ex);
	    ModelAndView mav = new ModelAndView();
	    mav.addObject("exception", ex);
	    mav.addObject("url", req.getRequestURL());
	    mav.setViewName("error");
	    return mav;
	  }
}
