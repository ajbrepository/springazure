
package azure.spring.boot.services;

import com.microsoft.azure.datalake.store.ADLException;
import com.microsoft.azure.datalake.store.ADLStoreClient;
import com.microsoft.azure.datalake.store.ContentSummary;
import com.microsoft.azure.datalake.store.DirectoryEntry;
import com.microsoft.azure.datalake.store.IfExists;
import com.microsoft.azure.datalake.store.oauth2.AccessTokenProvider;
import com.microsoft.azure.datalake.store.oauth2.ClientCredsTokenProvider;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.OperationContext;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.BlobContainerPublicAccessType;
import com.microsoft.azure.storage.blob.BlobRequestOptions;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.microsoft.azure.storage.blob.ListBlobItem;

import azure.spring.boot.pojo.Item;

import java.io.*;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Service;

@Service("AzureUtil")
public class AzureUtil {

	private static String clientId = "a5222d5d-9179-4302-b547-d2dfcadae98c";
	private static String authTokenEndpoint = "https://login.microsoftonline.com/6baac333-d950-40dc-bdf7-edecfe347a22/oauth2/token";
	private static String clientKey = "BxWKoztNYEpkng4vIV/6oC5ikzbxJTKVfxUVQgUEfk8=";
	// full account FQDN, not just the account name
	// private static String accountFQDN = "tcsdlstore.azuredatalakestore.net";
	private static String accountFQDN = "tcsdatalake.azuredatalakestore.net";
	private static String location = "/data/upload/";

	// Generic Storage String
	public static final String storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=tcsazure535storage;AccountKey=zCdtvQIvi+EtceRmYT2Nus3FX3Y0H/AxVbjW+hqqH6QwB855zvJORwzsIfNd6Z/pgGgMH32wOdYhshkJc7OmDw==;EndpointSuffix=core.windows.net";

	public static void main(String[] args) {
		System.out.println("Program started");
		try {
			// new AzureUtil().createDirectory();
			// new AzureUtil().getContents();
			System.out.println(new AzureUtil().getBlobStorageContents());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void createDirectory() throws Exception {
		AccessTokenProvider provider = new ClientCredsTokenProvider(authTokenEndpoint, clientId, clientKey);
		try {
			provider.getToken();

			ADLStoreClient client = ADLStoreClient.createClient(accountFQDN, provider);

			// create directory
			client.createDirectory(location + "Test");
			// client.delete("data");
			System.out.println("Directory created.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void createFile(byte[] bytes, String fileName) throws Exception {
		AccessTokenProvider provider = new ClientCredsTokenProvider(authTokenEndpoint, clientId, clientKey);
		try {
			ADLStoreClient client = ADLStoreClient.createClient(accountFQDN, provider);
			// create File
			OutputStream stream = client.createFile(location + fileName, IfExists.OVERWRITE);
			// create file using byte arrays
			// byte[] buf = getBytesFromInputStream(is);
			stream.write(bytes);
			stream.close();
			System.out.println("File created using byte array.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<Item> getContents() {

		AccessTokenProvider provider = new ClientCredsTokenProvider(authTokenEndpoint, clientId, clientKey);
		List<Item> results = new ArrayList<Item>();
		try {
			ADLStoreClient client = ADLStoreClient.createClient(accountFQDN, provider);
			// create directory
			List<DirectoryEntry> entries = client.enumerateDirectory(location);
			for (DirectoryEntry entry : entries) {
				results.add(new Item(entry.name, entry.type.toString()));
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return results;

	}

	public byte[] getBytesFromInputStream(InputStream is) throws IOException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		byte[] buffer = new byte[0xFFFF];
		for (int len = is.read(buffer); len != -1; len = is.read(buffer)) {
			os.write(buffer, 0, len);
		}
		return os.toByteArray();
	}

	public List<Item> getBlobStorageContents() throws InvalidKeyException, URISyntaxException, StorageException {

		List<Item> results = new ArrayList<Item>();
		File sourceFile = null, downloadedFile = null;
		System.out.println("Azure Blob storage quick start sample");

		CloudStorageAccount storageAccount;
		CloudBlobClient blobClient = null;
		CloudBlobContainer container = null;

		// Parse the connection string and create a blob client to interact with Blob
		// storage
		storageAccount = CloudStorageAccount.parse(storageConnectionString);
		blobClient = storageAccount.createCloudBlobClient();
		container = blobClient.getContainerReference("azureblobcontainer");

		// Create the container if it does not exist with public access.
		System.out.println("Creating container: " + container.getName());
		// container.createIfNotExists(BlobContainerPublicAccessType.CONTAINER, new
		// BlobRequestOptions(), new OperationContext());

		Iterator<ListBlobItem> iter = container.listBlobs().iterator();

		while (iter.hasNext()) {
			results.add(new Item(iter.next().getUri().toString(), "BLOB"));
		}
		return results;

	}

	public void createBlobStorageContent(InputStream inputStream,String fileName) throws InvalidKeyException, URISyntaxException, StorageException, IOException {

		List<Item> results = new ArrayList<Item>();
		File sourceFile = null, downloadedFile = null;
		System.out.println("Azure Blob storage quick start sample");

		CloudStorageAccount storageAccount;
		CloudBlobClient blobClient = null;
		CloudBlobContainer container = null;

		// Parse the connection string and create a blob client to interact with Blob
		// storage
		storageAccount = CloudStorageAccount.parse(storageConnectionString);
		blobClient = storageAccount.createCloudBlobClient();
		container = blobClient.getContainerReference("azureblobcontainer");

		// Create the container if it does not exist with public access.
		System.out.println("Creating container: " + container.getName());
		// container.createIfNotExists(BlobContainerPublicAccessType.CONTAINER, new
		// BlobRequestOptions(), new OperationContext());


		//Getting a blob reference
		CloudBlockBlob blob = container.getBlockBlobReference(fileName);

		//Creating blob and uploading file to it
		System.out.println("Uploading the sample file ");
		blob.upload(inputStream, 0);
		//blob.uploadFromFile(sourceFile.getAbsolutePath());

	}

}
