package azure.spring.boot.rest;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.microsoft.azure.storage.StorageException;

import azure.spring.boot.services.AzureUtil;
import azure.spring.boot.pojo.Item;

@RestController
public class FileUploadController {
	
	@Autowired
	private AzureUtil azureUtil;

	@PostMapping("/upload")
	public String handleFileUpload(@RequestParam("user-file") MultipartFile multipartFile) throws Exception {
		String name = multipartFile.getOriginalFilename();
		System.out.println("File name: " + name);
		// todo save to a file via multipartFile.getInputStream()
		byte[] bytes = multipartFile.getBytes();
		//System.out.println("File uploaded content:\n" + new String(bytes));
		azureUtil.createFile(bytes, multipartFile.getOriginalFilename());
		return "file uploaded";
	}
	
	@PostMapping("/uploadBlob")
	public String handleFileBlobUpload(@RequestParam("user-file") MultipartFile multipartFile) throws Exception {
		String name = multipartFile.getOriginalFilename();
		System.out.println("File name: " + name);
		// todo save to a file via multipartFile.getInputStream()
		byte[] bytes = multipartFile.getBytes();
		//System.out.println("File uploaded content:\n" + new String(bytes));
		azureUtil.createBlobStorageContent(multipartFile.getInputStream(),multipartFile.getOriginalFilename());
		return "file uploaded";
	}

	@RequestMapping("/contents")
	public List<Item> getContents(@RequestParam(value = "location", defaultValue = "") String name) throws InvalidKeyException, URISyntaxException, StorageException {
		//return azureUtil.getContents();
		return azureUtil.getBlobStorageContents();
	}

}