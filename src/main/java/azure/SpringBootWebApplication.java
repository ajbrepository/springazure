package azure;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

//@ComponentScan({ "azure.spring.boot.web.controller", "azure.spring.boot.rest", "azure.spring.boot.services" })

@SpringBootApplication
public class SpringBootWebApplication  {
	

    
    public static void main(String[] args) {
        SpringApplication.run(SpringBootWebApplication.class, args);
    }
}